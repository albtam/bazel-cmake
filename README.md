# bazel-cmake

Generate a C++ library with Bazel and link against it with CMake.

```bash
$ cd cool_lib
$ export CoolLib_ROOT=$(pwd)
$ bazel build //lib:cool_lib
$ cd ../cool_executable
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./cool_executable
```