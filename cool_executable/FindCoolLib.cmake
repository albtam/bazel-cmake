if(NOT CoolLib_ROOT)
  set(CoolLib_ROOT "$ENV{CoolLib_ROOT}")
endif()

set(_CoolLib_ROOT "${CoolLib_ROOT}")
message(_CoolLib_ROOT: ${_CoolLib_ROOT})

find_path(CoolLib_INCLUDE_DIRS NAMES add_numbers.hpp HINTS ${_CoolLib_ROOT}/bazel-cool_lib/lib/)
message(CoolLib_INCLUDE_DIRS: ${CoolLib_INCLUDE_DIRS})

find_library(CoolLib_LIBRARIES NAMES cool_lib HINTS ${_CoolLib_ROOT}/bazel-bin/lib)
message(CoolLib_LIBRARIES: ${CoolLib_LIBRARIES})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(CoolLib
  FOUND_VAR
    CoolLib_FOUND
  REQUIRED_VARS
    CoolLib_INCLUDE_DIRS
    CoolLib_LIBRARIES
  )
